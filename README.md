# CRUD Test app

## Install

- Create docker containers

```shell
docker-compose up -d
```

- Copy the .env file with environment 

```shell
docker-compose exec app cp .env.example .env
```

- Install dependencies

```shell
docker-compose exec app composer update
```

- Create the database structure and the demo data

```shell
docker-compose exec app php artisan init
```

- Run tests

```shell
docker-compose exec app php ./vendor/phpunit/phpunit/phpunit --configuration ./phpunit.xml
```

## Use

All queries must contain a token header

### list

get list of articles

```shell
curl --location \
--request GET 'http://localhost/api/articles?page=1&limit=10&sort_by=created_at&sort_dir=desc' \
--header 'token: test_auth_string'
```

Params

- page (int) - number of the selected page (default - 1)

- limit (int) - amount of articles per page (default - 10)

- sort_by (string) - column for sorting (default - created_at)

- sort_dir (string) - direction of sorting (default - desc)

### item

get a single article

```shell
curl --location \
--request GET 'http://localhost/api/articles/{{id}}' \
--header 'token: test_auth_string'
```

Params

- id (int) - id of the selected article

### store

crate a new article

```shell
curl --location \
--request POST 'http://localhost/api/articles' \
--header 'X-Requested-With: XMLHttpRequest' \
--header 'token: test_auth_string' \
--form 'title=""' \
--form 'body=""'
```

Headers

- X-Requested-With: XMLHttpRequest - for correct validation work

Params

- title (string) - the article caption; required; max 200 chars

- body (string) - the article body; required; max: 10000 chars

### update

edit the article

```shell
curl --location \
--request PUT 'http://localhost/api/articles/{{id}}' \
--header 'X-Requested-With: XMLHttpRequest' \
--header 'token: test_auth_string' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'title=' \
--data-urlencode 'body='
```

Headers

- X-Requested-With: XMLHttpRequest - for correct validation work

- Content-Type: application/x-www-form-urlencoded - for correct field data transfer

Params

- id (int) - id of the selected article

- title (string) - the article caption; required; max 200 chars

- body (string) - the article body; required; max: 10000 chars

### delete

delete the single article

```shell 
curl --location \
--request DELETE 'http://localhost/api/articles/{{id}}' \
--header 'token: test_auth_string'
```

Params

- id (int) - id of the selected article

## Postman

Postman collection included

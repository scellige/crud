<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Run protected or private methods from class instance
     *
     * @param mixed $instance instance of the required class
     * @param string $method_name
     * @param array $arguments
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public function invokeMethod(&$instance, $method_name, array $arguments = [])
    {
        $reflection = new \ReflectionClass(get_class($instance));
        $method = $reflection->getMethod($method_name);
        $method->setAccessible(true);

        return $method->invokeArgs($instance, $arguments);
    }
}

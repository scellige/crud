<?php

namespace Tests\Feature;

use App\Http\Controllers\Article;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Check index method work and base behavior
     *
     * @return void
     */
    public function testIndexBase()
    {
        (new DatabaseSeeder())->run();

        $response = $this
            ->get('/api/articles', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);
    }

    /**
     * Check index method work with wrong token value
     *
     * @return void
     */
    public function testIndexWrongToken()
    {
        (new DatabaseSeeder())->run();

        $response = $this
            ->get('/api/articles', [
                'token' => 'wrong_token',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(401);
    }

    /**
     * Check index method work with pagination param
     *
     * @return void
     */
    public function testIndexPagination()
    {
        (new DatabaseSeeder())->run();

        $response = $this
            ->get('/api/articles?page=2', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);
    }

    /**
     * Check index method work with limit param (acceptable value)
     *
     * @return void
     */
    public function testIndexLimit()
    {
        (new DatabaseSeeder())->run();

        $response = $this
            ->get('/api/articles?limit=20', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);
    }

    /**
     * Check index method work with sort params
     *
     * @return void
     */
    public function testIndexSort()
    {
        (new DatabaseSeeder())->run();

        $response = $this
            ->get('/api/articles?sort_by=title&sort_dir=asc', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);
    }

    /**
     * Check index method work with unacceptable limit param value
     *
     * @return void
     */
    public function testIndexQueryValidation()
    {
        (new DatabaseSeeder())->run();

        $response = $this
            ->get('/api/articles?limit=200', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(302);
    }

    /**
     * Check store method work
     *
     * @return void
     */
    public function testStoreBase()
    {
        (new DatabaseSeeder())->run();

        //create an article
        $response = $this
            ->postJson('/api/articles', [
                'title' => 'article title',
                'body' => 'article body',
            ], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);

        //try to create article with the same title
        $response = $this
            ->postJson('/api/articles', [
                'title' => 'article title',
                'body' => 'article body',
            ], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(422);
    }

    /**
     * Check show method work
     *
     * @return void
     */
    public function testShowBase()
    {
        //try to get nonexistent article
        $response = $this
            ->get('/api/articles/1', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(404);

        (new DatabaseSeeder())->run();

        //get article with id = 1
        $response = $this
            ->get('/api/articles/1', [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);
    }

    /**
     * Check update method work
     *
     * @return void
     */
    public function testUpdateBase()
    {
        (new DatabaseSeeder())->run();

        //change data of article
        $response = $this
            ->putJson('/api/articles/1', [
                'title' => 'article title',
                'body' => 'article body',
            ], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);

        //change only body with the same title for same article
        $response = $this
            ->putJson('/api/articles/1', [
                'title' => 'article title',
                'body' => 'article body alter value',
            ], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);

        //change other article with the same title
        $response = $this
            ->putJson('/api/articles/2', [
                'title' => 'article title',
                'body' => 'article body',
            ], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(422);
    }

    /**
     * Check delete method work
     *
     * @return void
     */
    public function testDelete()
    {
        (new DatabaseSeeder())->run();

        //delete an article
        $response = $this
            ->delete('/api/articles/1', [], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(200);

        //delete an nonexistent article
        $response = $this
            ->delete('/api/articles/1', [], [
                'token' => 'different_testing_token_string',
                'x-requested-with' => 'XMLHttpRequest',
                'content-type' => 'application/json',
            ]);

        $response->assertStatus(404);
    }

    /**
     * Check array to xml convertation
     *
     * @throws \ReflectionException
     */
    public function testToXML()
    {
        $instance = new Article();
        $xml = new \SimpleXMLElement('<?xml version="1.0"?><root></root>');

        $data = [
            'string_node' => 'simple_node_value',
            'number_node' => 123,
            'simple_array_node' => [
                'value_1',
                'value_2',
                222,
            ],
            'array_node' => [
                'key_1' => 'value_1',
                'key_2' => 'value_2',
                'key_3' => [
                    1, 2, 3,
                ],
            ],
        ];

        $right = <<<XML
<?xml version="1.0"?>
<root>
    <string_node>simple_node_value</string_node>
    <number_node>123</number_node>
    <simple_array_node>
        <item>value_1</item>
        <item>value_2</item>
        <item>222</item>
    </simple_array_node>
    <array_node>
        <key_1>value_1</key_1>
        <key_2>value_2</key_2>
        <key_3>
            <item>1</item>
            <item>2</item>
            <item>3</item>
        </key_3>
    </array_node>
</root>
XML;

        $this->invokeMethod($instance, 'toXML', [$data, &$xml]);

        $this->assertEquals($xml, (new \SimpleXMLElement($right)));
        $this->assertNotEquals($xml, (new \SimpleXMLElement('<?xml version="1.0"?><root></root>')));
    }
}

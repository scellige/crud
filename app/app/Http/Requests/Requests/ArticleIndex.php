<?php

namespace App\Http\Requests\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleIndex extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sort_by' => 'nullable|string|max:20',
            'sort_dir' => 'nullable|string|max:4',
            'limit' => 'nullable|int|max:20',
            'page' => 'nullable|int',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Requests\ArticleIndex as ArticleIndexRequest;
use App\Http\Requests\Requests\ArticleStore as ArticleStoreRequest;
use App\Models\Article as ArticleModel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use SimpleXMLElement as XML;

class Article extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ArticleIndexRequest $request)
    {
        $list = ArticleModel::orderBy(
            $request->sort_by ?? 'created_at',
            $request->sort_dir ?? 'desc'
        )
            ->paginate($request->limit ?? 10);

        return $this->response($list->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArticleStoreRequest $request)
    {
        $item = ArticleModel::create($request->validated());

        return $this->response($item->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id)
    {
        $item = ArticleModel::findOrFail($id);

        return $this->response($item->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required|string|max:200|unique:articles,title,' . $id,
            'body' => 'required|string|max:10000',
        ]);

        $item = ArticleModel::findOrFail($id);

        $item->update($request->only(['title', 'body']));

        return $this->response($item->toArray());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        $item = ArticleModel::findOrFail($id);

        $item->delete();

        return $this->response();
    }

    /**
     * Response in selected format (xml is selected or json as default)
     *
     * @param array|null $data
     * @param bool $is_xml
     *
     * @return JsonResponse
     */
    protected function response(?array $data = null, bool $is_xml = false)
    {
        if ($is_xml) {
            $xml = new XML('<?xml version="1.0"?><root></root>');

            $this->toXML($data, $xml);

            $result = response($xml->asXML());
        } else {
            $result = response()->json($data);
        }

        return $result;
    }

    /**
     * Convert data from array to xml document
     *
     * @param array $data
     * @param XML $xml
     *
     * @return void
     */
    protected function toXML(array $data, XML &$xml)
    {
        foreach ($data as $k => $v) {
            if (is_numeric($k)) {
                $k = 'item';
            }

            if (is_array($v)) {
                $node = $xml->addChild($k);
                $this->toXML($v, $node);
            } else {
                $xml->addChild((string) $k, htmlspecialchars($v));
            }
        }
    }
}

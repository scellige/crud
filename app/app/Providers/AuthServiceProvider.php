<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerCustomAuth();
    }

    /**
     * Register custom auth method
     *
     * @return void
     */
    public function registerCustomAuth()
    {
        Auth::viaRequest('custom-token', function (Request $request) {
            $token = $request->header('token');

            if (empty($token) || $token !== config('auth.base_token')) {
                return null;
            }

            return true;
        });
    }
}
